import { css } from "@emotion/react";
import {
  Box,
  Drawer,
  IconButton,
  Paper,
  Typography,
  useMediaQuery,
} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import { useState } from "react";

import DrawerContent from "../common/components/DrawerContent";
import { theme } from "../theme";

interface Props {
  children: React.ReactNode;
}

const BaseLayout = ({ children }: Props) => {
  const [isDrawerOpen, setIsDrawerOpen] = useState(false);

  const isDesktop = useMediaQuery(theme.breakpoints.up("md"));

  const handleDrawerToggle = () => setIsDrawerOpen((state) => !state);

  const container = window !== undefined ? () => document.body : undefined;

  return (
    <div css={styles.root}>
      <Paper css={styles.header}>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          edge="start"
          onClick={handleDrawerToggle}
          css={styles.drawerButton}
        >
          <MenuIcon />
        </IconButton>
        <Typography variant="h1">Coolmovies</Typography>
      </Paper>
      <div css={styles.content}>
        <Paper css={isDesktop && { ...styles.drawer }}>
          {isDesktop ? (
            <DrawerContent />
          ) : (
            <Drawer
              PaperProps={{
                style: {
                  width: "100%",
                  maxWidth: 230,
                  backgroundColor: theme.palette.primary.main,
                  borderRadius: 0,
                },
              }}
              container={container}
              open={isDrawerOpen}
              variant={"temporary"}
              onClose={handleDrawerToggle}
              ModalProps={{
                keepMounted: true,
              }}
              sx={{
                display: { xs: "block", md: "none" },
              }}
            >
              <DrawerContent />
            </Drawer>
          )}
        </Paper>
        <Box css={styles.pageContent}>{children}</Box>
      </div>
    </div>
  );
};

const styles = {
  root: css({
    height: "100vh",
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  }),
  header: css({
    width: "100%",
    height: 60,

    display: "flex",
    alignItems: "center",

    borderRadius: 0,

    backgroundColor: theme.palette.primary.dark,
    color: theme.palette.primary.contrastText,
    h1: {
      fontWeight: 300,

      [theme.breakpoints.up("md")]: {
        marginLeft: 60,
      },
    },
  }),
  content: css({
    width: "100vw",
    height: "calc(100vh - 60px)",
    display: "flex",
    justifyContent: "flex-start",
  }),
  pageContent: css({
    width: "100%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    overflowX: "hidden",
    overflowY: "auto",
  }),
  drawer: css({
    width: "100%",
    maxWidth: 230,
    backgroundColor: theme.palette.primary.main,
    borderRadius: 0,
  }),
  drawerButton: css({
    margin: "0 10px",

    [theme.breakpoints.up("md")]: {
      display: "none",
    },
  }),
};

export default BaseLayout;
