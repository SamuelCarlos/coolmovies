export const MOCKED_MOVIE: { movieById: Movies.Data } = {
  movieById: {
    imgUrl: "/img.jpg",
    movieDirectorByMovieDirectorId: {
      name: "Director Name",
      age: 12,
      id: "testID",
    },
    id: "id1",
    releaseDate: "2022-02-02",
    title: "test movie",
    userByUserCreatorId: {
      id: "testCreatorID",
      name: "test creator",
    },
  },
};
