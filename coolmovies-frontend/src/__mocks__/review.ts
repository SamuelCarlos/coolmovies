export const MOCKED_REVIEW: Reviews.Data = {
  id: "review-1",
  body: "body of an awesome testing review",
  rating: 1,
  title: "awesome testing review",
  userByUserReviewerId: {
    id: "reviewer-1",
    name: "reviewer",
  },
};

export const MOCK_REVIEW_LIST: Paging<Reviews.Data> = {
  nodes: [{ ...MOCKED_REVIEW }],
  pageInfo: {
    endCursor: "review-1",
    startCursor: "review-1",
    hasNextPage: false,
    hasPreviousPage: false,
  },
  totalCount: 1,
};
