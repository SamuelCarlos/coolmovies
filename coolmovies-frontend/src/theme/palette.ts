import { ThemeOptions } from "@mui/material";

export const defaultPalette = {
  primary: {
    light: "#64da73",
    main: "#28a745",
    dark: "#007717",
    contrastText: "#ffffff",
  },
  secondary: {
    light: "#fff350",
    main: "#ffc107",
    dark: "#c79100",
    contrastText: "#ffffff",
  },
  success: {
    main: "#46D5B2",
  },
  error: {
    main: "#FF2626",
  },
  text: {
    primary: "#000",
    secondary: "#5C5C77",
  },
  info: {
    main: "#17a2b8",
  },
  background: {
    default: "#f9f9f9",
  },
} as ThemeOptions["palette"];
