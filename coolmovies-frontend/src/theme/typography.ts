import { ThemeOptions } from "@mui/material";

export default {
  fontFamily: "Roboto, sans-serif",
  color: "#454545",
  h1: {
    fontSize: "2rem",
    lineHeight: "80px",
    fontWeight: "500",
    fontStyle: "normal",
    "@media (min-width: 900px)": {
      fontSize: "2rem",
      lineHeight: "3.2rem",
    },
  },
  h2: {
    fontSize: "0.8rem",
    lineHeight: "24px",
    fontWeight: "bold",
    fontStyle: "normal",
    "@media (min-width: 900px)": {
      fontSize: "1rem",
      lineHeight: "24px",
    },
  },
  subtitle1: {
    fontStyle: "normal",
    fontWeight: "300",
    fontSize: "1rem",
    lineHeight: "2rem",
    "@media (min-width: 900px)": {
      fontSize: "1.4rem",
      lineHeight: "2.9rem",
    },
  },
  caption: {
    fontStyle: "normal",
    fontWeight: "300",
    fontSize: "14px",
    lineHeight: "38px",
    "@media (min-width: 900px)": {
      fontSize: "12px",
      lineHeight: "30px",
    },
  },
} as ThemeOptions["typography"];
