import { createTheme } from "@mui/material";

import { defaultPalette } from "./palette";
import typography from "./typography";

const themeOptions = {
  typography,
  palette: defaultPalette,
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 900,
      lg: 1200,
      xl: 1536,
    },
  },
};

export const theme = createTheme({ ...themeOptions, palette: defaultPalette });
