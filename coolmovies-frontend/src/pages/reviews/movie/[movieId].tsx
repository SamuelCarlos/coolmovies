import { css } from "@emotion/react";
import { Button, Divider, Paper, Typography } from "@mui/material";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import AddRoundedIcon from "@mui/icons-material/AddRounded";

import BaseLayout from "../../../layouts/BaseLayout";
import ReviewCard from "../../../modules/reviews/components/ReviewCard";
import { movieActions, reviewActions, useAppSelector } from "../../../redux";
import { theme } from "../../../theme";
import ReviewModal from "../../../modules/reviews/components/ReviewModal";

const MovieReviews = () => {
  const [movie, setMovie] = useState<Movies.Data>();
  const [reviewId, setReviewId] = useState<string | null>();
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);

  const dispatch = useDispatch();
  const movieState = useAppSelector((state) => state.movie);
  const router = useRouter();

  const handleOpenModal = () => setIsModalOpen(true);
  const handleCloseModal = () => {
    setReviewId(null);
    setIsModalOpen(false);
  };

  useEffect(() => {
    if (!router.query.movieId) return;

    if (!movieState.selected) {
      dispatch(movieActions.fetch({ movieId: router.query.movieId as string }));
    }

    if (movieState.selected) {
      dispatch(reviewActions.setMovieId({ movieId: movieState.selected }));
    }
  }, [dispatch, movieState.selected, router.query.movieId]);

  useEffect(() => {
    if (!movieState.data) {
      return;
    }

    setMovie(movieState.data);
  }, [movieState, movieState.data]);

  useEffect(() => {
    if (reviewId) {
      handleOpenModal();
    }
  }, [reviewId]);

  if (!movie) {
    return <BaseLayout>Loading...</BaseLayout>;
  }

  return (
    <BaseLayout>
      <Paper css={styles.container} square elevation={3}>
        <Paper
          css={styles.image}
          style={{ backgroundImage: `url(${movie.imgUrl})` }}
        />
        <div css={styles.information} data-testid="movie-info">
          <Divider />
          <Typography>
            Title:
            <b> {movie.title}</b>
          </Typography>
          <Divider />
          <Typography data-testid="director-name">
            Director:
            <b> {movie.movieDirectorByMovieDirectorId.name}</b>
          </Typography>
          <Divider />
          <Typography>
            Released at:
            <b> {new Date(movie.releaseDate).toLocaleDateString("en-US")}</b>
          </Typography>
          <Divider />
          <Button
            variant="contained"
            color="primary"
            css={styles.addButton}
            onClick={handleOpenModal}
            data-testid="addReviewButton"
          >
            <AddRoundedIcon /> Add your review
          </Button>
        </div>
      </Paper>
      <div css={reviewsStyles.container}>
        <Typography css={reviewsStyles.title}>
          {movieState.reviews?.totalCount} Reviews:
        </Typography>
        <div>{movieState.loadingReviews && "Loading reviews..."}</div>
        {!movieState.loadingReviews &&
          movieState.reviews?.nodes.map((review) => (
            <ReviewCard
              key={review.id}
              review={review}
              handleEdit={(reviewId) => {
                setReviewId(reviewId);
              }}
            />
          ))}
      </div>
      <ReviewModal
        isOpen={isModalOpen}
        handleClose={handleCloseModal}
        review={movieState.reviews?.nodes.find(
          (review) => review.id === reviewId
        )}
      />
    </BaseLayout>
  );
};

const styles = {
  container: css({
    width: "100%",

    display: "flex",
    flexDirection: "column",
    alignItems: "center",

    padding: 20,

    backgroundColor: theme.palette.grey[200],

    [theme.breakpoints.up("md")]: {
      backgroundColor: theme.palette.grey[100],
      flexDirection: "row",
      justifyContent: "center",
      alignItems: "flex-start",
    },
  }),
  image: css({
    width: 130,
    height: 205,

    backgroundRepeat: "no-repeat",
    backgroundPosition: "top",
    backgroundSize: "cover",

    boxShadow: "0px 0px 10px 1px rgba(0, 0, 0, 0.2)",

    [theme.breakpoints.up("md")]: {
      marginRight: 60,
      width: 260,
      height: 410,
    },
  }),
  information: css({
    paddingTop: 20,

    "& .MuiTypography-root": {
      color: theme.palette.text.secondary,

      fontSize: "1.2rem",
      margin: "5px 0",
    },

    [theme.breakpoints.up("md")]: {
      display: "flex",
      flexDirection: "column",
      alignSelf: "center",
    },
  }),
  addButton: css({
    marginTop: 10,
  }),
};

const reviewsStyles = {
  container: css({
    width: "100%",

    display: "flex",
    flexDirection: "column",
    alignItems: "center",

    flexGrow: 1,

    overflowX: "hidden",
    overflowY: "auto",

    padding: 20,

    backgroundColor: theme.palette.grey[100],
  }),
  title: css({
    alignSelf: "flex-start",
    fontSize: "1.2rem",
    margin: "5px 0",
    color: theme.palette.primary.main,

    [theme.breakpoints.up("md")]: {
      alignSelf: "center",
    },
  }),
};

export default MovieReviews;
