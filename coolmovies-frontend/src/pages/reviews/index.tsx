import { css } from "@emotion/react";
import { useEffect } from "react";
import BaseLayout from "../../layouts/BaseLayout";
import MovieCard from "../../modules/reviews/components/MovieCard";
import { allMoviesActions, useAppDispatch, useAppSelector } from "../../redux";

const Reviews = () => {
  const dispatch = useAppDispatch();
  const allMoviesState = useAppSelector((state) => state.allMovies);

  useEffect(() => {
    dispatch(allMoviesActions.fetchAll());
  }, [dispatch]);

  return (
    <BaseLayout>
      <div css={styles.content} data-testid="reviews">
        {allMoviesState.count > 0 &&
          allMoviesState.data?.map((movie, index) => (
            <MovieCard
              index={index + 1}
              key={movie.id}
              imageUrl={movie.imgUrl}
              movieId={movie.id}
              release={movie.releaseDate}
              title={movie.title}
              directorName={movie.movieDirectorByMovieDirectorId.name || ""}
            />
          ))}
      </div>
    </BaseLayout>
  );
};

const styles = {
  content: css({
    maxWidth: "100vw",
    width: "100%",

    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    flexWrap: "wrap",

    margin: 20,
  }),
};

export default Reviews;
