interface PageInfo {
  hasPreviousPage: boolean;
  hasNextPage: boolean;
  startCursor: string;
  endCursor: string;
}

interface Paging<T> {
  totalCount: number;
  pageInfo: PageInfo;
  nodes: T[];
}
