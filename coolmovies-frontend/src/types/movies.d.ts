namespace Movies {
  interface Director {
    id: string;
    name: string;
    age: number;
  }

  interface Data {
    id: string;
    imgUrl: string;
    releaseDate: string;
    title: string;
    movieDirectorByMovieDirectorId: Partial<Director>;
    userByUserCreatorId: Partial<Users.Data>;
  }

  type AllMovies = Paging<Data>;
}
