namespace Reviews {
  interface Data {
    id: string;
    title: string;
    body: string;
    rating: number;
    userByUserReviewerId: Users.Data;
  }

  interface Mutation {
    title: string;
    body: string;
    rating: number;
    name: string;
  }
}
