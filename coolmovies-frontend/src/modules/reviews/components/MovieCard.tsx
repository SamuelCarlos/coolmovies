import { css } from "@emotion/react";
import {
  Button,
  Card,
  CardActions,
  CardContent,
  Grow,
  Typography,
} from "@mui/material";
import { theme } from "../../../theme";
import ArrowForwardRoundedIcon from "@mui/icons-material/ArrowForwardRounded";
import { movieActions, useAppDispatch } from "../../../redux";
import { useRouter } from "next/router";

interface Props {
  imageUrl: string;
  movieId: string;
  release: string;
  title: string;
  directorName: string;
  index: number;
}

const MovieCard = ({
  imageUrl,
  index,
  title,
  movieId,
  release,
  directorName,
}: Props) => {
  const dispatch = useAppDispatch();
  const router = useRouter();

  const handleMovieSelect = (movieId: string) => {
    dispatch(movieActions.fetch({ movieId }));

    router.push(`/reviews/movie/${movieId}`);
  };

  return (
    <Grow
      in={Boolean(index)}
      {...(Boolean(index) ? { timeout: index * 300 } : {})}
    >
      <Card
        css={styles.container}
        style={{ backgroundImage: `url(${imageUrl})` }}
      >
        <div css={{ ...styles.image }} data-testid="moviecard">
          <CardContent css={styles.content}>
            <div>
              <Typography variant="h2" css={styles.title}>
                {`${title} - ${new Date(release).getFullYear()}`}
              </Typography>
              <Typography>{directorName}</Typography>
            </div>
            <CardActions css={styles.actions}>
              <Button
                variant="text"
                color="inherit"
                onClick={() => handleMovieSelect(movieId)}
                data-testid="see-reviews-button"
              >
                See reviews <ArrowForwardRoundedIcon />
              </Button>
            </CardActions>
          </CardContent>
        </div>
      </Card>
    </Grow>
  );
};

const styles = {
  container: css({
    width: "100%",
    height: 200,
    maxHeight: 200,

    position: "relative",

    margin: "5px 5px 0 5px",

    color: theme.palette.primary.contrastText,

    [theme.breakpoints.up("sm")]: {
      flexBasis: 400,
    },

    backgroundSize: "100%",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center",
  }),
  image: css({
    width: "100%",
    height: 200,

    display: "flex",
    justifyContent: "space-between",

    backgroundImage:
      "linear-gradient(to right, #00000099,#000000aa , #000000aa)",

    "&:hover": {
      backgroundImage:
        "linear-gradient(to right, #00000099,#000000aa , #000000dd)",
    },
  }),
  content: css({
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    justifyContent: "space-between",
    flexGrow: 1,
  }),
  title: css({
    fontSize: "1.1rem",
  }),
  actions: css({
    alignSelf: "flex-end",
    padding: 0,
    margin: 0,

    "& .MuiButton-root": {
      padding: 0,
      margin: 0,
      lineHeight: 0,
    },
  }),
};

export default MovieCard;
