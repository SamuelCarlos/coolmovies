import { css } from "@emotion/react";
import { Button, Paper, Typography } from "@mui/material";
import { useState } from "react";
import StarIcon from "@mui/icons-material/Star";
import StarBorderIcon from "@mui/icons-material/StarBorder";

import { theme } from "../../../theme";
import Image from "next/image";

interface Props {
  review: Reviews.Data;
  handleEdit: (id: string) => void;
}

const ReviewCard = ({ review, handleEdit }: Props) => {
  const [showAll, setShowAll] = useState(false);

  const toggleShowReview = () => setShowAll((state) => !state);

  const stars = Array.from({ length: 5 }, (_, index) =>
    index < review.rating ? true : false
  );

  return (
    <Paper css={styles.review} elevation={3} data-testid="review-card">
      <div css={styles.sentBy}>
        <Typography variant="body2">
          Sent by: <b>{review.userByUserReviewerId?.name}</b>
        </Typography>
        <div css={styles.stars}>
          {stars.map((star, index) =>
            star ? <StarIcon key={index} /> : <StarBorderIcon key={index} />
          )}
        </div>
      </div>
      <div css={styles.reviewBody}>
        <div css={styles.reviewTitle}>
          <Typography data-testid="reviewTitle">{review.title}</Typography>
        </div>
        <div>
          {review.body.length > 100 ? (
            <Typography>
              — &quot;
              {showAll ? review.body : review.body.slice(0, 100) + "..."}
              &quot;
            </Typography>
          ) : (
            <Typography>
              — &quot;{review.body}
              &quot;
            </Typography>
          )}
        </div>
        {review.body.length > 100 && (
          <Button
            css={styles.button}
            color="inherit"
            onClick={toggleShowReview}
            data-testid="toggleShowReview"
          >
            {showAll ? "show less" : "show more"}
          </Button>
        )}
      </div>
      <div css={styles.edit}>
        <Button
          onClick={() => handleEdit(review.id)}
          data-testid="editReviewButton"
        >
          <Image src="/static/edit.svg" alt="Edit icon" layout="fill" />
        </Button>
      </div>
    </Paper>
  );
};

const styles = {
  review: css({
    width: "100%",

    display: "flex",
    flexDirection: "column",

    padding: 20,
    marginBottom: 10,

    backgroundColor: theme.palette.grey[300],
    color: theme.palette.text.secondary,

    [theme.breakpoints.up("md")]: {
      maxWidth: 800,
    },
  }),
  sentBy: css({
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    paddingBottom: 10,
  }),
  stars: css({
    display: "flex",

    color: theme.palette.primary.main,
  }),
  reviewBody: css({
    padding: 10,
    borderRadius: 10,

    display: "flex",
    flexDirection: "column",

    boxShadow: "inset 0px 0px 10px 0px rgba(0,0,0,0.2)",
    backgroundColor: theme.palette.grey[500],
    color: theme.palette.grey[200],
  }),
  reviewTitle: css({
    "& .MuiTypography-root": {
      fontWeight: 500,
      color: theme.palette.primary.contrastText,
    },
  }),
  button: css({
    marginTop: 10,
    padding: 0,
    fontWeight: 300,
    fontSize: "0.7rem",
    alignSelf: "flex-end",
  }),
  edit: css({
    width: "100%",

    display: "flex",
    justifyContent: "flex-end",

    "& .MuiButton-root": {
      marginTop: 10,
      width: 20,
      height: 20,
    },
  }),
};

export default ReviewCard;
