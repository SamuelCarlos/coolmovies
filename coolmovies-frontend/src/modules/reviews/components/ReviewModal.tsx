import { css } from "@emotion/react";
import {
  Zoom,
  Modal,
  Paper,
  Slider,
  TextField,
  Typography,
  IconButton,
  Button,
} from "@mui/material";
import StarIcon from "@mui/icons-material/Star";
import StarBorderIcon from "@mui/icons-material/StarBorder";
import CloseRoundedIcon from "@mui/icons-material/CloseRounded";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";

import { theme } from "../../../theme";
import { reviewActions } from "../../../redux";

interface Props {
  isOpen: boolean;
  handleClose: () => void;
  review?: Reviews.Data;
}

const MARKS = [
  {
    value: 1,
    label: "1",
  },
  {
    value: 2,
    label: "2",
  },
  {
    value: 3,
    label: "3",
  },
  {
    value: 4,
    label: "4",
  },
  {
    value: 5,
    label: "5",
  },
];

const EMPTY_FORM = {
  title: "",
  body: "",
  rating: 3,
  name: "",
};

const ReviewModal = ({ isOpen, handleClose, review }: Props) => {
  const [formData, setFormData] = useState<Reviews.Mutation>(EMPTY_FORM);

  const dispatch = useDispatch();

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setFormData((state) => ({ ...state, [e.target.name]: e.target.value }));
  };

  const handleSliderChange = (_: Event, value: number | number[]) => {
    setFormData((state) => ({ ...state, rating: value as number }));
  };

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    if (review) {
      const updateData: Reviews.Data = {
        body: formData.body,
        id: review.id,
        title: formData.title,
        rating: formData.rating,
        userByUserReviewerId: { ...review?.userByUserReviewerId },
      };

      dispatch(reviewActions.update(updateData));
    } else {
      dispatch(reviewActions.create(formData));
    }

    handleClose();
  };

  useEffect(() => {
    if (review) {
      setFormData({
        title: review.title,
        body: review.body,
        rating: review.rating,
        name: review.userByUserReviewerId.name,
      });
    }
  }, [review]);

  return (
    <Modal
      open={isOpen}
      onClose={handleClose}
      css={styles.modal}
      data-testid="modal"
    >
      <Zoom in={isOpen}>
        <Paper css={styles.content}>
          <div css={styles.topbar} data-testid="modalData">
            <Typography variant="h2">Review</Typography>
            <IconButton onClick={handleClose} data-testid="close-button">
              <CloseRoundedIcon />
            </IconButton>
          </div>
          <form css={styles.form} onSubmit={handleSubmit}>
            <TextField
              label="Your name"
              name="name"
              fullWidth
              required
              onChange={handleChange}
              value={formData.name}
              disabled={Boolean(review)}
            />

            <div css={styles.sliderWrapper}>
              <Typography>Rating</Typography>

              <div css={styles.slider}>
                <StarBorderIcon css={styles.star} className="left" />
                <Slider
                  value={formData.rating}
                  max={5}
                  min={1}
                  name="rating"
                  aria-label="Default"
                  valueLabelDisplay="auto"
                  onChange={handleSliderChange}
                  marks={MARKS}
                />
                <StarIcon css={styles.star} className="right" />
              </div>
            </div>

            <TextField
              label="Review title"
              name="title"
              fullWidth
              required
              onChange={handleChange}
              inputProps={{ "data-testid": "titleInput" }}
              value={formData.title}
            />

            <TextField
              label="Your review"
              name="body"
              multiline
              fullWidth
              rows={4}
              required
              onChange={handleChange}
              value={formData.body}
            />
            <div css={styles.actions}>
              <Button
                color="error"
                onClick={handleClose}
                data-testid="cancel-button"
              >
                Cancel
              </Button>
              <Button
                variant="contained"
                type="submit"
                data-testid="submitReview"
              >
                Submit
              </Button>
            </div>
          </form>
        </Paper>
      </Zoom>
    </Modal>
  );
};

const styles = {
  modal: css({
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  }),
  topbar: css({
    height: 50,
    width: "100%",

    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",

    svg: {
      transition: "all 250ms ease-in",
      color: theme.palette.error.dark,
    },

    "&:hover": {
      svg: {
        transform: "rotate(90deg)",
      },
    },
  }),
  content: css({
    width: "100%",
    maxWidth: 800,
    padding: "10px 20px 20px 20px",
  }),
  form: css({
    width: "100%",

    paddingTop: 10,

    display: "flex",
    flexDirection: "column",

    justifyContent: "center",
    alignItems: "center",

    "& *": {
      marginBottom: 10,
    },
  }),
  sliderWrapper: css({
    width: "100%",

    "& .MuiTypography-root": {
      paddingLeft: 15,
      fontSize: "0.7rem",
      color: theme.palette.grey[700],
    },
  }),
  slider: css({
    display: "flex",
    position: "relative",
  }),
  star: css({
    position: "absolute",

    color: theme.palette.primary.main,

    top: -17,

    "&.left": {
      left: -12,
    },

    "&.right": {
      right: -12,
    },
  }),
  actions: css({
    width: "100%",

    display: "flex",
    justifyContent: "space-between",
  }),
};

export default ReviewModal;
