import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface MoviesState {
  count: number;
  pageInfo?: PageInfo;
  data?: Movies.Data[];
}

const initialState: MoviesState = {
  count: 0,
};

export const slice = createSlice({
  initialState,
  name: "allMovies",
  reducers: {
    fetchAll: () => {},
    clearData: (state: MoviesState) => {
      state.data = undefined;
      state.pageInfo = undefined;
      state.count = 0;
    },
    loaded: (
      state: MoviesState,
      action: PayloadAction<{ data: Movies.AllMovies }>
    ) => {
      state.data = action.payload.data.nodes;
      state.count = action.payload.data.totalCount;
      state.pageInfo = action.payload.data.pageInfo;
    },
    loadError: (state: MoviesState) => {
      state.data = undefined;
      state.pageInfo = undefined;
      state.count = -1;
    },
  },
});

export const { actions } = slice;
export type SliceAction = typeof actions;
export default slice.reducer;
