export { actions as allMoviesActions } from "./slice";
export { default as allMoviesReducer } from "./slice";
import { combineEpics } from "redux-observable";
import { moviesFetchEpic } from "./epics";

export const allMoviesEpics = combineEpics(moviesFetchEpic);
