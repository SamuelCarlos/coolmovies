import { gql } from "@apollo/client";
import { Epic, StateObservable } from "redux-observable";
import { Observable } from "rxjs";
import { filter, switchMap } from "rxjs/operators";
import { RootState } from "../../store";
import { EpicDependencies } from "../../types";
import { actions, SliceAction } from "./slice";

export const moviesFetchEpic: Epic = (
  action$: Observable<SliceAction["fetchAll"]>,
  state$: StateObservable<RootState>,
  { client }: EpicDependencies
) =>
  action$.pipe(
    filter(actions.fetchAll.match),
    switchMap(async () => {
      try {
        const result = await client.query<{ allMovies: Movies.AllMovies }>({
          query: moviesQuery,
        });
        return actions.loaded({ data: result.data.allMovies });
      } catch (err) {
        return actions.loadError();
      }
    })
  );

export const moviesQuery = gql`
  query AllMovies {
    allMovies {
      nodes {
        imgUrl
        movieDirectorByMovieDirectorId {
          name
          nodeId
        }
        id
        releaseDate
        title
        userByUserCreatorId {
          name
        }
      }
      totalCount
      pageInfo {
        hasPreviousPage
        hasNextPage
        startCursor
        endCursor
      }
    }
  }
`;
