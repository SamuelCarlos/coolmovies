import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface MovieState {
  selected?: string;
  data?: Movies.Data;
  loadingReviews: boolean;
  reviews?: Paging<Reviews.Data>;
  error?: string;
}

const initialState: MovieState = {
  selected: undefined,
  data: undefined,
  loadingReviews: true,
  reviews: undefined,
  error: undefined,
};

export const slice = createSlice({
  initialState,
  name: "movies",
  reducers: {
    fetch: (state: MovieState, action: PayloadAction<{ movieId: string }>) => {
      state.selected = action.payload.movieId;
    },
    clearData: (state: MovieState) => {
      state.data = undefined;
      state.selected = undefined;
      state.reviews = undefined;
    },
    loaded: (
      state: MovieState,
      action: PayloadAction<{ data: Movies.Data }>
    ) => {
      state.data = { ...action.payload.data };
    },
    loadedReviews: (
      state: MovieState,
      action: PayloadAction<{ data: Paging<Reviews.Data> }>
    ) => {
      state.reviews = { ...action.payload.data };
      state.loadingReviews = false;
    },
    loadError: (state: MovieState) => {
      state.data = undefined;
      state.selected = undefined;
      state.error = "Error while fetching movie data. :(";
    },
    loadReviewsError: (state: MovieState) => {
      state.reviews = undefined;
      state.error = "Error while fetching movie reviews. :(";
    },
  },
});

export const { actions } = slice;
export type SliceAction = typeof actions;
export default slice.reducer;
