import { Observable } from "rxjs";
import { gql } from "@apollo/client";
import { filter, switchMap } from "rxjs/operators";
import { Epic, StateObservable } from "redux-observable";

import { RootState } from "../../store";
import { EpicDependencies } from "../../types";
import { actions, SliceAction } from "./slice";

export const movieFetchEpic: Epic = (
  action$: Observable<SliceAction["fetch"]>,
  state$: StateObservable<RootState>,
  { client }: EpicDependencies
) =>
  action$.pipe(
    filter(actions.fetch.match),
    switchMap(async () => {
      if (!state$.value.movie.selected) return actions.loadError();

      try {
        const result = await client.query({
          query: movieByIdQuery,
          variables: { id: state$.value.movie.selected },
        });
        return actions.loaded({ data: result.data.movieById });
      } catch (err) {
        return actions.loadError();
      }
    })
  );

export const fetchReviewsEpic: Epic = (
  action$: Observable<SliceAction["fetch"]>,
  state$: StateObservable<RootState>,
  { client }: EpicDependencies
) =>
  action$.pipe(
    filter(actions.fetch.match),
    switchMap(async () => {
      try {
        const result = await client.query<{
          allMovieReviews: Paging<Reviews.Data>;
        }>({
          query: movieReviewsByMovieIdQuery,
          variables: { id: state$.value.movie.selected },
          fetchPolicy: "network-only",
        });
        return actions.loadedReviews({ data: result.data.allMovieReviews });
      } catch (err) {
        return actions.loadReviewsError();
      }
    })
  );

export const movieByIdQuery = gql`
  query MovieById($id: UUID!) {
    movieById(id: $id) {
      id
      imgUrl
      title
      releaseDate
      userByUserCreatorId {
        name
        id
      }
      movieDirectorByMovieDirectorId {
        id
        age
        name
      }
    }
  }
`;

export const movieReviewsByMovieIdQuery = gql`
  query MovieReviewsByMovieId($id: UUID!) {
    allMovieReviews(filter: { movieId: { equalTo: $id } }) {
      nodes {
        body
        id
        title
        rating
        userByUserReviewerId {
          name
          id
        }
      }
      totalCount
      pageInfo {
        endCursor
        hasNextPage
        hasPreviousPage
        startCursor
      }
    }
  }
`;
