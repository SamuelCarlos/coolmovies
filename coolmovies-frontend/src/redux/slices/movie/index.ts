export { actions as movieActions } from "./slice";
export { default as movieReducer } from "./slice";
import { combineEpics } from "redux-observable";
import { fetchReviewsEpic, movieFetchEpic } from "./epics";

export const movieEpics = combineEpics(movieFetchEpic, fetchReviewsEpic);
