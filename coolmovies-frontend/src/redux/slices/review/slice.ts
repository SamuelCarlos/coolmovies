import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface ReviewState {
  review?: Reviews.Data;
  newReview?: Reviews.Mutation;
  movieId?: string;
  error?: string;
}

const initialState: ReviewState = {
  review: undefined,
  newReview: undefined,
  movieId: undefined,
  error: undefined,
};

export const slice = createSlice({
  initialState,
  name: "reviews",
  reducers: {
    setMovieId: (
      state: ReviewState,
      action: PayloadAction<{ movieId: string }>
    ) => {
      state.movieId = action.payload.movieId;
    },
    create: (state: ReviewState, action: PayloadAction<Reviews.Mutation>) => {
      state.newReview = { ...action.payload };
    },
    update: (state: ReviewState, action: PayloadAction<Reviews.Data>) => {
      state.review = { ...action.payload };
    },
    created: (state: ReviewState, action: PayloadAction<Reviews.Data>) => {
      state.review = { ...action.payload };
      state.newReview = undefined;
    },
    createError: (
      state: ReviewState,
      action: PayloadAction<{ error: string }>
    ) => {
      state.error = action.payload.error;
    },
  },
});

export const { actions } = slice;
export type SliceAction = typeof actions;
export default slice.reducer;
