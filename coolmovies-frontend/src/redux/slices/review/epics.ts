import { gql } from "@apollo/client";
import { Epic, StateObservable } from "redux-observable";
import { Observable } from "rxjs";
import { filter, switchMap, map } from "rxjs/operators";
import { RootState } from "../../store";
import { EpicDependencies } from "../../types";
import { movieActions } from "../movie";
import { actions, SliceAction } from "./slice";

export const createReviewEpic: Epic = (
  action$: Observable<SliceAction["create"]>,
  state$: StateObservable<RootState>,
  { client }: EpicDependencies
) =>
  action$.pipe(
    filter(actions.create.match),
    switchMap(async () => {
      const { newReview, movieId } = state$.value.review;

      if (!newReview)
        return actions.createError({
          error: "New review data is not defined.",
        });
      if (!movieId)
        return actions.createError({ error: "Movie ID is not defined." });

      try {
        const user = await client.mutate<{ createUser: { user: Users.Data } }>({
          mutation: createUserMutation,
          variables: { name: newReview.name },
        });

        if (!user.data?.createUser.user) {
          return actions.createError({ error: "Failed to create an user." });
        }

        const result = await client.mutate<{
          createMovieReview: { movieReview: Reviews.Data };
        }>({
          mutation: createReviewMutation,
          variables: {
            body: newReview.body,
            userReviewerId: user.data.createUser.user.id,
            rating: newReview.rating,
            title: newReview.title,
            movieId,
          },
        });

        if (!result.data?.createMovieReview.movieReview)
          return actions.createError({ error: "Error while creating review." });

        return actions.created({
          ...result.data.createMovieReview.movieReview,
        });
      } catch (err) {
        return actions.createError({ error: JSON.stringify(err, null, 2) });
      }
    })
  );

export const updateReviewEpic: Epic = (
  action$: Observable<SliceAction["update"]>,
  state$: StateObservable<RootState>,
  { client }: EpicDependencies
) =>
  action$.pipe(
    filter(actions.update.match),
    switchMap(async () => {
      const { review } = state$.value.review;

      if (!review) return actions.createError({ error: "No review passed." });

      try {
        const result = await client.mutate<{
          updateMovieReviewById: { movieReview: Reviews.Data };
        }>({
          mutation: updateReviewMutation,
          variables: {
            body: review.body,
            rating: review.rating,
            title: review.title,
            id: review.id,
          },
        });

        if (!result.data?.updateMovieReviewById.movieReview)
          return actions.createError({ error: "Error while updating review." });

        return actions.created({
          ...result.data.updateMovieReviewById.movieReview,
        });
      } catch (err) {
        console.log(err);
        return actions.createError({ error: JSON.stringify(err, null, 2) });
      }
    })
  );

export const reloadReviewsEpic: Epic = (
  action$: Observable<SliceAction["created"]>,
  state$: StateObservable<RootState>
) =>
  action$.pipe(
    filter(actions.created.match),
    map(() =>
      movieActions.fetch({ movieId: state$.value.movie.selected || "" })
    )
  );

export const createUserMutation = gql`
  mutation CreateUser($name: String!) {
    createUser(input: { user: { name: $name } }) {
      user {
        id
        name
      }
    }
  }
`;

export const createReviewMutation = gql`
  mutation CreateMovieReview(
    $body: String!
    $movieId: UUID!
    $title: String!
    $rating: Int!
    $userReviewerId: UUID!
  ) {
    createMovieReview(
      input: {
        movieReview: {
          body: $body
          title: $title
          rating: $rating
          movieId: $movieId
          userReviewerId: $userReviewerId
        }
      }
    ) {
      movieReview {
        id
        body
        rating
        title
        userByUserReviewerId {
          name
          id
        }
        movieId
      }
    }
  }
`;

export const updateReviewMutation = gql`
  mutation UpdateMoviewReviewById(
    $body: String!
    $title: String!
    $rating: Int!
    $id: UUID!
  ) {
    updateMovieReviewById(
      input: {
        movieReviewPatch: { body: $body, rating: $rating, title: $title }
        id: $id
      }
    ) {
      movieReview {
        id
        body
        rating
        title
        userByUserReviewerId {
          name
          id
        }
      }
    }
  }
`;
