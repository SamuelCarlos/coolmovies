export { actions as reviewActions } from "./slice";
export { default as reviewReducer } from "./slice";
import { combineEpics } from "redux-observable";
import { createReviewEpic, reloadReviewsEpic, updateReviewEpic } from "./epics";

export const reviewEpics = combineEpics(
  createReviewEpic,
  reloadReviewsEpic,
  updateReviewEpic
);
