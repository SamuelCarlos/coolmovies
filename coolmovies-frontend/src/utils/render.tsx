import { ThemeProvider } from "@mui/material";
import { EnhancedStore } from "@reduxjs/toolkit";
import { render } from "@testing-library/react";
import { Provider } from "react-redux";

import { theme } from "../theme";

const Render = (children: React.ReactNode, store: EnhancedStore) => {
  return render(
    <Provider store={store}>
      <ThemeProvider theme={theme}>{children}</ThemeProvider>
    </Provider>
  );
};

export default Render;
