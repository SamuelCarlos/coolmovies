import { DocumentNode, InMemoryCache } from "@apollo/client";
import {
  createMockClient as createMockApolloClient,
  RequestHandler,
} from "mock-apollo-client";

const defaultCacheOptions = {
  fragmentMatcher: { match: () => true },
  addTypename: false,
};

export function createMockClient(
  handlers: { query: DocumentNode; value: RequestHandler<any, any> }[],
  resolvers = {},
  cacheOptions = {}
) {
  const cache = new InMemoryCache({
    ...defaultCacheOptions,
    ...cacheOptions,
  });

  const mockClient = createMockApolloClient({ cache, resolvers });

  if (Array.isArray(handlers)) {
    handlers.forEach(({ query, value }) =>
      mockClient.setRequestHandler(query, value)
    );
  }

  return mockClient;
}
