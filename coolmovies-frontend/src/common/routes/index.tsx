import FormatListBulletedIcon from "@mui/icons-material/FormatListBulleted";
import BarChaartIcon from "@mui/icons-material/BarChart";

export const routes = [
  {
    path: "/reviews",
    name: "All Movies",
    icon: () => <BarChaartIcon />,
  },
];
