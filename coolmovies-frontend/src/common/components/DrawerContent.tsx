import { css } from "@emotion/react";
import {
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
} from "@mui/material";
import Link from "next/link";
import { useRouter } from "next/router";
import { theme } from "../../theme";
import { routes } from "../routes";

const DrawerContent = () => {
  const router = useRouter();

  return (
    <List css={styles.list}>
      {routes.map((route) => (
        <ListItem
          disablePadding
          key={route.name}
          sx={
            router.asPath === route.path
              ? { backgroundColor: theme.palette.primary.dark }
              : {}
          }
        >
          <Link href={route.path} passHref>
            <ListItemButton>
              <ListItemIcon css={styles.icon}>{route.icon()}</ListItemIcon>
              <ListItemText>{route.name}</ListItemText>
            </ListItemButton>
          </Link>
        </ListItem>
      ))}
    </List>
  );
};

const styles = {
  list: css({
    color: theme.palette.primary.contrastText,

    svg: {
      color: theme.palette.primary.contrastText,
    },
  }),
  icon: css({
    width: 20,
  }),
};

export default DrawerContent;
