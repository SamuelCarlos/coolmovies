import { ApolloClient } from "@apollo/client";
import { act, RenderResult, screen } from "@testing-library/react";

import ReviewCard from "../../../../src/modules/reviews/components/ReviewCard";
import { createStore } from "../../../../src/redux";
import Render from "../../../../src/utils/render";
import { createMockClient } from "../../../../src/utils/mockApolloHelper";
import { movieByIdQuery } from "../../../../src/redux/slices/movie/epics";
import { MOCKED_REVIEW } from "../../../../src/__mocks__/review";

const MOCK_BODY_MORE_THAN_100_CHARS =
  "This is a mock with more than 100 chars on, mock mock mock mock mock mock mock mock mock mock mock mock";

const client = new ApolloClient(
  createMockClient([
    {
      query: movieByIdQuery,
      value: () =>
        Promise.resolve({
          data: { ...MOCKED_REVIEW, body: MOCK_BODY_MORE_THAN_100_CHARS },
        }),
    },
  ])
);

const mockRouterPush = jest.fn();

jest.mock("next/router", () => ({
  useRouter: () => ({ push: mockRouterPush }),
}));

const mockHandleEdit = jest.fn();

describe("<ReviewCard />", () => {
  const store = createStore({ epicDependencies: { client } });

  let renderResult: RenderResult;

  beforeEach(async () => {
    await act(async () => {
      renderResult = Render(
        <ReviewCard
          review={{ ...MOCKED_REVIEW, body: MOCK_BODY_MORE_THAN_100_CHARS }}
          handleEdit={mockHandleEdit}
        />,
        store
      );
    });
  });

  it("Should render", async () => {
    expect(screen.getByTestId("review-card")).toBeDefined();
  });

  describe("Toggle show review button", () => {
    it("Dont should render if review.body more than 100 characters", () => {
      expect(MOCK_BODY_MORE_THAN_100_CHARS.length).toBeGreaterThan(100);

      expect(screen.getByTestId("toggleShowReview")).toBeDefined();
    });

    it("Should render if review.body have 100 or less characters", async () => {
      await act(async () => {
        renderResult?.rerender(
          <ReviewCard review={MOCKED_REVIEW} handleEdit={mockHandleEdit} />
        );
      });
      expect(screen.queryByTestId("toggleShowReview")).not.toBeInTheDocument();
    });
  });
});
