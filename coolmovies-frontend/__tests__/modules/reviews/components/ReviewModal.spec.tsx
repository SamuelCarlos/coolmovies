import { ApolloClient } from "@apollo/client";
import { act, fireEvent, screen } from "@testing-library/react";

import ReviewModal from "../../../../src/modules/reviews/components/ReviewModal";
import { createStore } from "../../../../src/redux";
import Render from "../../../../src/utils/render";
import { createMockClient } from "../../../../src/utils/mockApolloHelper";
import { MOCKED_REVIEW } from "../../../../src/__mocks__/review";
import {
  createReviewMutation,
  createUserMutation,
  updateReviewMutation,
} from "../../../../src/redux/slices/review/epics";

const UPDATED_TITLE = "New updated title";

const client = new ApolloClient(
  createMockClient([
    {
      query: createUserMutation,
      value: () =>
        Promise.resolve({
          data: {
            user: MOCKED_REVIEW.userByUserReviewerId,
          },
        }),
    },
    {
      query: createReviewMutation,
      value: () =>
        Promise.resolve({
          data: {
            createMovieReview: {
              movieReview: MOCKED_REVIEW,
            },
          },
        }),
    },
    {
      query: updateReviewMutation,
      value: () =>
        Promise.resolve({
          data: {
            updateMovieReviewById: {
              movieReview: { ...MOCKED_REVIEW, title: UPDATED_TITLE },
            },
          },
        }),
    },
  ])
);

const mockRouterPush = jest.fn();

jest.mock("next/router", () => ({
  useRouter: () => ({ push: mockRouterPush }),
}));

describe("<ReviewModal />", () => {
  let isOpen = true;
  const handleClose = () => {
    isOpen = false;
  };

  beforeEach(async () => {
    await act(async () => {
      const store = createStore({ epicDependencies: { client } });

      Render(
        <ReviewModal
          isOpen={isOpen}
          handleClose={handleClose}
          review={MOCKED_REVIEW}
        />,
        store
      );
    });
  });
  it("Should render", async () => {
    expect(screen.getByTestId("modalData")).toBeDefined();
  });

  it("Should render a submit review button", () => {
    expect(screen.getByTestId("submitReview")).toBeDefined();
  });

  it("Should change review title", async () => {
    const titleInput = screen.getByTestId<HTMLInputElement>("titleInput");

    expect(titleInput).toBeInTheDocument();

    fireEvent.change(titleInput, { target: { value: UPDATED_TITLE } });

    const submitButton = screen.getByTestId("submitReview");

    fireEvent.click(submitButton);

    expect(titleInput.value).toBe(UPDATED_TITLE);
  });
});
