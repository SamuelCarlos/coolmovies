import { ApolloClient } from "@apollo/client";
import { act, fireEvent, screen } from "@testing-library/react";

import MovieCard from "../../../../src/modules/reviews/components/MovieCard";
import { createStore } from "../../../../src/redux";
import Render from "../../../../src/utils/render";
import { createMockClient } from "../../../../src/utils/mockApolloHelper";
import { movieByIdQuery } from "../../../../src/redux/slices/movie/epics";
import { MOCKED_MOVIE } from "../../../../src/__mocks__/movie";

const client = new ApolloClient(
  createMockClient([
    {
      query: movieByIdQuery,
      value: () =>
        Promise.resolve({
          data: MOCKED_MOVIE,
        }),
    },
  ])
);

const mockRouterPush = jest.fn();

jest.mock("next/router", () => ({
  useRouter: () => ({ push: mockRouterPush }),
}));

describe("<MovieCard />", () => {
  beforeEach(async () => {
    await act(async () => {
      const store = createStore({ epicDependencies: { client } });

      const { imgUrl, movieDirectorByMovieDirectorId, id, releaseDate, title } =
        MOCKED_MOVIE.movieById;

      Render(
        <MovieCard
          imageUrl={imgUrl}
          directorName={movieDirectorByMovieDirectorId.name || ""}
          index={1}
          movieId={id}
          release={releaseDate}
          title={title}
        />,
        store
      );
    });
  });
  it("Should render", async () => {
    expect(screen.getByTestId("moviecard")).toBeDefined();
  });

  it("Should render a See reviews button", () => {
    expect(screen.getByTestId("see-reviews-button")).toBeDefined();
  });

  it("Should call router.push with movieId as param when click on see reviews", () => {
    const seeReviewsButton = screen.getByTestId("see-reviews-button");

    fireEvent.click(seeReviewsButton);

    expect(mockRouterPush).toHaveBeenCalledWith(
      `/reviews/movie/${MOCKED_MOVIE.movieById.id}`
    );
  });
});
