import React from "react";
import { act } from "react-dom/test-utils";
import { ApolloClient } from "@apollo/client";
import { screen } from "@testing-library/react";

import Reviews from "../../../src/pages/reviews";

import Render from "../../../src/utils/render";
import { createStore } from "../../../src/redux";
import { moviesQuery } from "../../../src/redux/slices/allMovies/epics";
import { createMockClient } from "../../../src/utils/mockApolloHelper";

const MOCKED_MOVIES = {
  allMovies: {
    nodes: [
      {
        imgUrl: "/img.jpg",
        movieDirectorByMovieDirectorId: {
          name: "test",
          nodeId: "testID",
        },
        id: "id1",
        releaseDate: "2022-02-02",
        title: "test movie",
        userByUserCreatorId: {
          name: "test creator",
        },
      },
    ],
    totalCount: 1,
    pageInfo: {
      hasPreviousPage: false,
      hasNextPage: false,
      startCursor: "id1",
      endCursor: "id1",
    },
  },
};

const useRouter = jest.spyOn(require("next/router"), "useRouter");

const client = new ApolloClient(
  createMockClient([
    {
      query: moviesQuery,
      value: () =>
        Promise.resolve({
          data: MOCKED_MOVIES,
        }),
    },
  ])
);

describe("<Reviews />", () => {
  beforeEach(() => {
    useRouter.mockImplementation(() => ({
      asPath: "/reviews",
    }));
  });

  it("Should render one movie card", async () => {
    await act(async () => {
      const store = createStore({ epicDependencies: { client } });

      Render(<Reviews />, store);
    });

    expect(screen.getByTestId("reviews")).toBeDefined();
    expect(screen.getByTestId("reviews").childElementCount).toBe(1);
    expect(screen.getByTestId("moviecard")).toBeDefined();
  });
});
