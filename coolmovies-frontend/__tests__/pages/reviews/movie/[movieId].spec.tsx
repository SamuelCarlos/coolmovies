import React from "react";
import { act } from "react-dom/test-utils";
import { ApolloClient } from "@apollo/client";
import { fireEvent, screen, waitFor } from "@testing-library/react";

import MovieReviews from "../../../../src/pages/reviews/movie/[movieId]";

import Render from "../../../../src/utils/render";
import { createStore } from "../../../../src/redux";
import { createMockClient } from "../../../../src/utils/mockApolloHelper";
import {
  movieByIdQuery,
  movieReviewsByMovieIdQuery,
} from "../../../../src/redux/slices/movie/epics";
import { MOCKED_MOVIE } from "../../../../src/__mocks__/movie";
import { MOCK_REVIEW_LIST } from "../../../../src/__mocks__/review";

const useRouter = jest.spyOn(require("next/router"), "useRouter");

const client = new ApolloClient(
  createMockClient([
    {
      query: movieByIdQuery,
      value: () =>
        Promise.resolve({
          data: MOCKED_MOVIE,
        }),
    },
    {
      query: movieReviewsByMovieIdQuery,
      value: () =>
        Promise.resolve({
          data: { allMovieReviews: MOCK_REVIEW_LIST },
        }),
    },
  ])
);

describe("<MovieReviews />", () => {
  beforeEach(async () => {
    useRouter.mockImplementation(() => ({
      asPath: "/reviews/movie/id1",
      query: { movieId: "id1" },
    }));

    await act(async () => {
      const store = createStore({ epicDependencies: { client } });

      Render(<MovieReviews />, store);
    });
  });

  it("Should render", async () => {
    expect(screen.getByTestId("movie-info")).toBeDefined();
    expect(screen.getByTestId("director-name")).toBeDefined();
    expect(screen.getByTestId("director-name").textContent).toBe(
      `Director: ${MOCKED_MOVIE.movieById.movieDirectorByMovieDirectorId.name}`
    );
  });

  it("Should render an add review button", async () => {
    const button = screen.getByTestId("addReviewButton");

    expect(button).toBeDefined();
  });

  it("Should open a modal when add review button is clicked", async () => {
    const button = screen.getByTestId("addReviewButton");

    expect(button).toBeDefined();

    fireEvent.click(button);

    expect(screen.queryByTestId("modalData")).toBeInTheDocument();
  });

  it("Should close the modal when close button is clicked", async () => {
    const button = screen.getByTestId("addReviewButton");

    fireEvent.click(button);

    expect(screen.queryByTestId("modalData")).toBeInTheDocument();

    const closeButton = screen.queryByTestId<HTMLButtonElement>("close-button");

    expect(closeButton).toBeInTheDocument();

    if (closeButton) fireEvent.click(closeButton);

    await waitFor(() =>
      expect(screen.queryByTestId("close-button")).not.toBeInTheDocument()
    );
  });

  it("Should open a fulfilled modal when click on edit review", async () => {
    const editButton = screen.queryByTestId("editReviewButton");

    expect(editButton).toBeInTheDocument();

    if (editButton) fireEvent.click(editButton);

    expect(screen.queryByTestId("modalData")).toBeInTheDocument();
  });
});
